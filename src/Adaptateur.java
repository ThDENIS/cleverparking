import java.io.BufferedReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Adaptateur {
	
	public ArrayList<Capteur> capteurs; 
	
	public Adaptateur() throws IOException{
		this.capteurs = new ArrayList<Capteur>();
		this.capteurs.add(lectureFichier("ressources/capteur1.txt"));
		this.capteurs.add(lectureFichier("ressources/capteur2.txt"));
		this.capteurs.add(lectureFichier("ressources/capteur3.txt"));
		
	}
	
	public void lanceCapteurs() {
		for (Capteur capteur : this.capteurs) {
			capteur.start(); 
		}
	}
	
	public Capteur lectureFichier(String path) throws IOException {
		try {
			BufferedReader buffer = new BufferedReader(new FileReader(path)); 
			String[] params = new String[3];
			String lecture;  
			int cpt = 0; 
			while ((lecture = buffer.readLine()) != null) {
				String[] split = lecture.split(":"); 
				params[cpt] = split[1];
				cpt++; 
			}
			String pathSensor = "ressources/" +  params[2].substring(params[2].length()-1, params[2].length()) + ".txt"; 
			return new Capteur(Integer.parseInt(params[0]), Integer.parseInt(params[1]), pathSensor); 
			
		}
		catch (FileNotFoundException e) {
			System.out.println("Fichier capteur non trouv� !");
		}
		return null; 
				
	}
	
	public ArrayList<Capteur> getCapteurs(){
		return this.capteurs; 
	}
	
	

}
