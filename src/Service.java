
public class Service {

	private Capteur capteur; 
	private int filtre; 
	private String lastValue; 
	
	public Service(Capteur capteur, int filtre) {
		this.capteur = capteur; 
		this.filtre = filtre; 
		lastValue = ""; 
	}
	
	public String showValues() {
		String value = "";
		value += "Capteur : " + this.capteur.getSid() + "\n"; 
		value += "value : "; 
		switch (this.filtre) {
			case 0 : 
				value += Integer.toString(this.capteur.getEtat());  
				break; 
			case 1 : 
				value += Filters.f1(this.capteur); 
				break; 
			case 2 : 
				if (Filters.f2(Integer.toString(this.capteur.getEtat()), this.lastValue)) {
					value += Integer.toString(this.capteur.getEtat());
					this.lastValue = Integer.toString(this.capteur.getEtat()); 
				}
					
				else 
					return "";
				break; 
			case 12 : 
				if (Filters.f2(Filters.f1(this.capteur), this.lastValue)) {
					value += Filters.f1(this.capteur); 
					this.lastValue = Filters.f1(this.capteur); 
				}
				else 
					return ""; 
				break; 
		}
		
		value += "\n"; 
		return value; 

	}
	
	
}
