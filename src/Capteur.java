import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Capteur extends Thread {

	private int sid;
	private int frequence;
	private int etat;
	private String path;


	public Capteur(int sid, int frequence, String path) {
		this.sid = sid;
		this.frequence = frequence;
		this.path = path;
		this.etat = -1; 
	}
	


	public Integer getSid() {
		return sid;
	}

	public int getEtat() {
		return etat;
	}

	public void setEtat(int etat) {
		this.etat = etat;
	}

	public int getFrequence() {
		return frequence;
	}

	public void setFrequence(Integer frequence) {
		this.frequence = frequence;
	}

	public String getPath() {
		return path;
	}

	public String toString() {
		return this.getId()+"\r\n"+this.getFrequence()+"\r\n"+this.getEtat()+"\r\n";
	}
	
	public void run() {
		while(true) {
			try {
				lectureAutomatique();
				
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void lectureAutomatique() throws IOException, InterruptedException {
		//Ajouter gestion de la fr�quence
		lecture();
		Server.onLecture(this); 
//		System.out.println(this.sid + " : " + this.etat);
		Thread.sleep(frequence * 1000);
	}
	
	public void lecture() throws IOException {
		BufferedReader buffer = new BufferedReader(new FileReader(this.path)); 
		String value = "" ; 
		String lecture;  
		while ((lecture = buffer.readLine()) != null) {
			value = lecture;  
		}
		this.etat = Integer.parseInt(value); 
	}
}
