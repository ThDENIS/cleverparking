public class Filters {
	
	public static String f1(Capteur c) {
		if (c.getEtat() == -1)
			 return "Le capteur n'a pas encore ete initialise";
		else if (c.getEtat() == 1)
			return "D"; 
		else 
			return "N"; 
	}
	
	public static boolean f2(String value, String lastValue) {

		if (value.equals(lastValue))
			return false; 
		return true; 
	}	
}
