import java.io.File;
import java.io.IOException;

public class Server {
	
	private static Adaptateur adaptateur; 
	private static Service service1; 
	private static Service service2; 
	private static Service service3;
	

	public static void main(String[] args) throws IOException {
		
		adaptateur = new Adaptateur();
		service1 = new Service(adaptateur.getCapteurs().get(0), 0); 
		service2 = new Service(adaptateur.getCapteurs().get(1), 1); 
		service3 = new Service(adaptateur.getCapteurs().get(2), 12); 
		adaptateur.lanceCapteurs(); 
	}

	
	public static void onLecture(Capteur capteur) {
		int index = adaptateur.getCapteurs().indexOf(capteur); 
		if (index == 0)
			System.out.println(service1.showValues()); 
		else if (index == 1)
			System.out.println(service2.showValues()); 
		else if (index == 2)
			System.out.println(service3.showValues());
	}
}
